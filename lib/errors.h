#pragma once

#include <stdexcept>

struct IntervalDivisionByZero : public std::runtime_error {
    using std::runtime_error::runtime_error;
};

struct IncorrectInterval: public std::runtime_error {
    using std::runtime_error::runtime_error;
};

struct IncorrectArgument: public std::runtime_error {
    using std::runtime_error::runtime_error;
};

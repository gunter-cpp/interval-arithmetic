#pragma once

#include <utility>

#include "decimal.hh"

using decimal::Decimal;

class Interval {
public:
    explicit Interval(const Decimal& dot) : Interval(dot, dot) {
    }
    explicit Interval(const std::string& dot) : Interval(dot, dot) {
    }
    Interval(const std::string& left, const std::string& right)
        : Interval(Decimal(left), Decimal(right)) {
    }
    Interval(const Decimal& left, const Decimal& right);

    std::pair<Decimal, Decimal> endpoints() const;
    std::string to_string() const;
    Decimal wid() const;
    Interval intersect(const Interval& other) const;

    bool contains(const Decimal& other) const;
    bool contains(const Interval& other) const;

    bool is_intersecting(const Interval& other) const;
    bool is_dot() const;

    bool operator==(const Interval& other) const;
    Interval operator+(const Interval& other) const;
    Interval operator-() const;
    Interval operator-(const Interval& other) const;
    Interval operator*(const Interval& other) const;
    Interval operator/(const Interval& other) const;

    Interval pow(const std::string& power) const;
    Interval pow(const Interval& power) const;
    Interval pow(const Decimal& power) const;

    Interval exp() const;
    Interval ln() const;

private:
    Decimal left_;
    Decimal right_;
};

std::ostream& operator<<(std::ostream &out, const Interval& interval);

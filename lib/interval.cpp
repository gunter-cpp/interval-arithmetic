#include "interval.h"

#include "errors.h"

inline void set_round_floor() {
    decimal::context.allcr(true);
    decimal::context.round(decimal::ROUND_FLOOR);
}

inline void set_round_ceil() {
    decimal::context.allcr(true);
    decimal::context.round(decimal::ROUND_CEILING);
}

Interval::Interval(const Decimal& left, const Decimal& right) {
    if (right < left) {
        throw IncorrectInterval(
            "Left endpoint should be greater or equal to the left one."
            "Provided endpoints: " +
            left.to_eng() + ", " + right.to_eng());
    }
    left_ = left;
    right_ = right;
}
std::pair<Decimal, Decimal> Interval::endpoints() const {
    return {left_, right_};
}
bool Interval::contains(const Decimal& other) const {
    return contains(Interval(other));
}
bool Interval::contains(const Interval& other) const {
    return left_ <= other.left_ && other.right_ <= right_;
}
bool Interval::is_dot() const {
    return left_ == right_;
}
std::string Interval::to_string() const {
    return "[" + left_.to_eng() + ", " + right_.to_eng() + "]";
}
Decimal Interval::wid() const {
    return right_ - left_;
}
Interval Interval::intersect(const Interval& other) const {
    return {std::max(left_, other.left_), std::min(right_, other.right_)};
}
bool Interval::operator==(const Interval& other) const {
    return other.left_ == left_ && other.right_ == right_;
}
Interval Interval::operator+(const Interval& other) const {
    set_round_floor();
    auto new_left = left_ + other.left_;
    set_round_ceil();
    auto new_right = right_ + other.right_;
    return {new_left, new_right};
}
Interval Interval::operator-() const {
    set_round_floor();
    auto new_left = -right_;
    set_round_ceil();
    auto new_right = -left_;
    return {new_left, new_right};
}
Interval Interval::operator-(const Interval& other) const {
    return *this + (-other);
}
Interval Interval::operator*(const Interval& other) const {
    set_round_floor();
    auto aa = left_ * other.left_;
    auto ab = left_ * other.right_;
    auto ba = right_ * other.left_;
    auto bb = right_ * other.right_;
    auto new_left = std::min({aa, ab, ba, bb});
    set_round_ceil();
    aa = left_ * other.left_;
    ab = left_ * other.right_;
    ba = right_ * other.left_;
    bb = right_ * other.right_;
    auto new_right = std::max({aa, ab, ba, bb});
    return {new_left, new_right};
}
Interval Interval::operator/(const Interval& other) const {
    if (other.contains(Interval("0"))) {
        throw IntervalDivisionByZero("Divisor intervalcontains zero");
    }
    return *this * Interval(1 / other.right_, 1 / other.left_);
}
Interval Interval::pow(const std::string& power) const {
    return pow(Decimal(power));
}
Interval Interval::pow(const Interval& power) const {
    if (power.is_dot()) {
        return pow(power.left_);
    }
    throw IncorrectArgument("Only dot intervals are accepted as powers but " + power.to_string() +
                            " is given.");
}
Interval Interval::pow(const Decimal& power) const {
    if (power.exponent() < 0) {
        throw IncorrectArgument("Only integers are accepted as powers but " + power.to_eng() +
                                "is given.");
    }
    set_round_floor();
    auto left_power_floor = left_.pow(power);
    auto right_power_floor = right_.pow(power);
    set_round_ceil();
    auto left_power_ceil = left_.pow(power);
    auto right_power_ceil = right_.pow(power);

    Decimal new_left =
        std::min({left_power_floor, right_power_ceil, left_power_floor, left_power_ceil});
    Decimal new_right =
        std::max({left_power_floor, right_power_ceil, left_power_floor, left_power_ceil});

    if (power % 2 == 0 && this->contains(Decimal("0"))) {
        new_left = Decimal("0");
    }
    Interval result{new_left, new_right};
    return result;
}
Interval Interval::exp() const {
    set_round_floor();
    auto left = left_.exp();
    left -= Decimal("10").pow(left.exponent());
    set_round_ceil();
    auto right = right_.exp();
    right += Decimal("10").pow(right.exponent());
    return {left, right};
}
Interval Interval::ln() const {
    if (left_ <= 0) {
        throw IncorrectArgument("Log can be applied only to positive numbers but " +
                                left_.to_eng() + " is given.");
    }
    set_round_floor();
    auto left = left_.ln();
    left -= Decimal("10").pow(left.exponent());
    set_round_ceil();
    auto right = right_.ln();
    right += Decimal("10").pow(right.exponent());
    return {left, right};
}
bool Interval::is_intersecting(const Interval& other) const {
    return !(right_ < other.left_ || other.right_ < left_);
}

std::ostream& operator<<(std::ostream& out, const Interval& interval) {
    out << interval.to_string();
    return out;
}

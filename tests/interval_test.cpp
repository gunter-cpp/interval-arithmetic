#include <gtest/gtest.h>

#include "interval.h"
#include "errors.h"

using decimal::Decimal;

class IntervalTest : public testing::Test {
protected:
    IntervalTest() {
        decimal::context.prec(10);
    }
};

TEST_F(IntervalTest, Construction) {
    auto interval = Interval(Decimal("1"), Decimal("2"));
    auto [left, right]= interval.endpoints();
    EXPECT_EQ(Decimal("1"), left);
    EXPECT_EQ(Decimal("2"), right);
}

TEST_F(IntervalTest, DotFromDecimal) {
    auto interval = Interval(Decimal("1"));
    auto [left, right]= interval.endpoints();
    EXPECT_EQ(Decimal("1"), left);
    EXPECT_EQ(Decimal("1"), right);
}

TEST_F(IntervalTest, DotFromString) {
    auto interval = Interval("1");
    auto [left, right]= interval.endpoints();
    EXPECT_EQ(Decimal("1"), left);
    EXPECT_EQ(Decimal("1"), right);
}

TEST_F(IntervalTest, FromStrings) {
    auto interval = Interval("1", "2");
    auto [left, right]= interval.endpoints();
    EXPECT_EQ(Decimal("1"), left);
    EXPECT_EQ(Decimal("2"), right);
}

TEST_F(IntervalTest, Compare) {
    auto a = Interval("1", "2");
    auto b = Interval("1", "2");
    EXPECT_EQ(a, b);
}

TEST_F(IntervalTest, Includes) {
    auto a = Interval("0.9", "2.1");
    auto b = Interval("1", "2");
    EXPECT_TRUE(a.contains(b));
    EXPECT_FALSE(b.contains(a));
}

TEST_F(IntervalTest, Sum) {
    decimal::context.prec(1);
    auto a = Interval("0.9", "2.1");
    auto b = Interval("1", "2");
    EXPECT_TRUE((a + b).contains({"1.9", "4.1"}));
}


TEST_F(IntervalTest, UnaryMinus) {
    decimal::context.prec(1);
    auto a = Interval("0.9", "1.9");
    EXPECT_TRUE((-a).contains({"-1.9", "-0.9"}));
}

TEST_F(IntervalTest, Difference) {
    decimal::context.prec(1);
    auto a = Interval("1", "2");
    auto b = Interval("3.9", "4.1");
    EXPECT_TRUE((a - b).contains({"-3.1", "-1.9"}));
}

TEST_F(IntervalTest, Multiplication) {
    decimal::context.prec(1);
    auto a = Interval("1", "2");
    auto b = Interval("-1.1", "2.1");
    EXPECT_TRUE((a * b).contains({"-3", "5"}));
}

TEST_F(IntervalTest, Division) {
    auto a = Interval("1", "2");
    auto b = Interval("1", "2");
    EXPECT_TRUE((a / b).contains({"0.5", "1"}));
}

TEST_F(IntervalTest, DivisionByZero) {
    auto a = Interval("1", "2");
    auto b = Interval("-1", "2");
    ASSERT_THROW(a / b, IntervalDivisionByZero);
}

TEST_F(IntervalTest, ToString) {
    auto a = Interval("1", "2");
    ASSERT_EQ("[1, 2]", a.to_string());
}

TEST_F(IntervalTest, Width) {
    auto a = Interval("1", "2");
    ASSERT_EQ(Decimal("1"), a.wid());
}

TEST_F(IntervalTest, IsDot) {
    auto a = Interval("1");
    ASSERT_TRUE(a.is_dot());
}

TEST_F(IntervalTest, Intersect) {
    auto a = Interval("1", "3");
    auto b = Interval("-1", "2");
    ASSERT_EQ(Interval("1", "2"), a.intersect(b));
}

TEST_F(IntervalTest, NoIntersection) {
    auto a = Interval("1", "3");
    auto b = Interval("-2", "-1");
    ASSERT_THROW(a.intersect(b), IncorrectInterval);
}

TEST_F(IntervalTest, IsIntersecting) {
    auto a = Interval("1", "3");
    auto b = Interval("-2", "-1");
    ASSERT_FALSE(a.is_intersecting(b));

    auto c = Interval("1", "3");
    auto d = Interval("-2", "2");
    ASSERT_TRUE(c.is_intersecting(d));
}

TEST_F(IntervalTest, Pow) {
    auto positive = Interval("2", "3");
    EXPECT_EQ(Interval("4", "9"), positive.pow("2"));
    EXPECT_TRUE(positive.pow("-1").contains(Interval("0.3334", "0.5"))) << positive.pow("-1");

    auto negative = Interval("-3", "-2");
    EXPECT_EQ(Interval("4", "9"), negative.pow("2"));
    EXPECT_EQ(Interval("-27", "-8"), negative.pow("3"));
    EXPECT_TRUE(negative.pow("-1").contains(Interval("-0.5","-0.3334"))) << negative.pow("-1");

    auto mixed = Interval("-2", "3");
    EXPECT_EQ(Interval("0", "9"), mixed.pow("2"));
    EXPECT_EQ(Interval("-8", "27"), mixed.pow("3"));
    EXPECT_TRUE(mixed.pow("-1").contains(Interval("-0.5","0.3333")));
}

TEST_F(IntervalTest, Exp) {
    decimal::context.prec(3);
    auto a = Interval("1", "2");
    EXPECT_TRUE(a.exp().contains(Interval("2.719", "7.389"))) << a.exp();
}

TEST_F(IntervalTest, Log) {
    decimal::context.prec(4);
    auto a = Interval("0.5", "3");
    auto [left, right] = a.ln().endpoints();
    EXPECT_TRUE(a.ln().contains(Interval("-0.6932", "1.099"))) << a.ln();

    decimal::context.prec(4);
    auto b = Interval("0.5", "2");
    EXPECT_TRUE(b.ln().contains(Interval("-0.6932", "0.6932"))) << b.ln();
}

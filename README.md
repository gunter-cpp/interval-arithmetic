# interval-arithmetic

![coverage](https://gitlab.com/gunter-cpp/interval-arithmetic/badges/main/coverage.svg)
![pipeline](https://gitlab.com/gunter-cpp/interval-arithmetic/badges/main/pipeline.svg
)
![license](https://img.shields.io/gitlab/license/gunter-cpp%2Finterval-arithmetic
)

Purely educational project.

Library that provides Interval class and associated function for rigorous interval arithmetic. Based on mpdecimal library.